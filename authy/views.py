from django.shortcuts import render, redirect

def login(request):
    errorMessage = "";
    if "POST" in request.method:
        roll_number = request.POST.get("roll_number")
        password = request.POST.get("password")

        if len(roll_number) > 0 and len(password) > 0 and roll_number == password:
            return redirect("/enrollment")
        else:
            errorMessage = "Wrong Credentials. Please try again"
    return render(request, "auth/login.html", {"errorMessage" : errorMessage})
from django.db import models



class Course(models.Model):
    courseId = models.CharField(max_length=10)
    courseName = models.TextField()

    def __str__(self):
        return self.courseId + " - " + self.courseName

class Enrollment(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

class Registration(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
from django.shortcuts import render

from .models import Course, Enrollment, Registration

def index(request):
    return render(request, 'enrollment/index.html')

def enrollment(request):
    if 'POST' in request.method:
        selected_pk = int(request.POST.get('selected_pk'))
        mode = request.POST.get('mode')
        course_instance = Course.objects.get(pk=selected_pk)
        if mode == "enroll":
            Enrollment.objects.create(course=course_instance)
        else:
            enrollment_instance = Enrollment.objects.filter(course=course_instance)
            if enrollment_instance.exists():
                enrollment_instance.delete()
    course_list = []
    for course in Course.objects.all():
        temp = {}
        temp['pk'] = course.pk
        temp['courseId'] = course.courseId
        temp['courseName'] = course.courseName
        if (Enrollment.objects.filter(course=course).exists()):
            temp['already_enrolled'] = True
        else:
            temp['already_enrolled'] = False
        course_list.append(temp)
    return render(request, 'enrollment/view_enrollment_form.html', {
        'course_list': course_list,
    })


def register(request):
    if 'POST' in request.method:
        selected_pk = int(request.POST.get('selected_pk'))
        mode = request.POST.get('mode')
        course_instance = Course.objects.get(pk=selected_pk)
        if mode == "register":
            Registration.objects.create(course=course_instance)
        else:
            enrollment_instance = Registration.objects.filter(course=course_instance)
            if enrollment_instance.exists():
                enrollment_instance.delete()
    course_list = []
    for entry in Enrollment.objects.all():
        temp = {}
        temp['pk'] = entry.course.pk
        temp['courseId'] = entry.course.courseId
        temp['courseName'] = entry.course.courseName
        if (Registration.objects.filter(course=entry.course).exists()):
            temp['already_registerd'] = True
        else:
            temp['already_registerd'] = False
        course_list.append(temp)
    return render(request, 'enrollment/view_registration_form.html', {
        'course_list': course_list,
    })
from django.urls import path

from .views import enrollment, register, index

urlpatterns = [
    path('', index, name="index"),
    path('view_enrollment_form', enrollment, name='view_enrollment_form'),
    path('view_registration_form', register, name='view_registration_form'),
]